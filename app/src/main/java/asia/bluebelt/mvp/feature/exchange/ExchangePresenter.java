package asia.bluebelt.mvp.feature.exchange;


import asia.bluebelt.mvp.base.BasePresenter;

/**
 * Created by bluebelt on 3/6/18.
 */

public class ExchangePresenter extends BasePresenter<ExchangeContract.View> implements ExchangeContract.Presenter {


    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onClickABC() {
        //show UI
        getUi().showAppContent();
    }
}
