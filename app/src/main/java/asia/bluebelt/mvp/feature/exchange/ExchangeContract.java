package asia.bluebelt.mvp.feature.exchange;


import asia.bluebelt.mvp.base.BaseContract;

/**
 * Created by bluebelt on 3/6/18.
 */

public interface ExchangeContract {
    public interface View extends BaseContract.View {
        void showLoadingDialog(boolean show);
        void showToast(String error);
        void showAppContent();
        void onKeyboardShow(boolean show);
    }

    public interface Presenter extends BaseContract.Presenter {
        void onClickABC();
    }
}
