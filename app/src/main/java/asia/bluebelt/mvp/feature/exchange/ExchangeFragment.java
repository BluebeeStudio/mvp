package asia.bluebelt.mvp.feature.exchange;


import asia.bluebelt.mvp.base.BaseFragment;

/**
 * Created by bluebelt on 3/5/18.
 */

public class ExchangeFragment extends BaseFragment<ExchangePresenter,ExchangeContract.View> implements ExchangeContract.View {
    @Override
    public void onResume() {
        super.onResume();
        mFragmentDisplay.showToolbar(true);
        mFragmentDisplay.showNavigationBottom(true);
    }

    private void handleOnClick(){
        getPresenter().onClickABC();
    }

    @Override
    public void showLoadingDialog(boolean show) {

    }

    @Override
    public void showToast(String error) {

    }

    @Override
    public void showAppContent() {

    }

    @Override
    public void onKeyboardShow(boolean show) {

    }

    @Override
    public ExchangePresenter createPresenter() {
        return new ExchangePresenter();
    }

    @Override
    public ExchangeContract.View getUi() {
        return this;
    }
}
