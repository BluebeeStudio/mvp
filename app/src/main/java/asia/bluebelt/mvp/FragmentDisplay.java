package asia.bluebelt.mvp;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by bluebelt on 3/5/18.
 */

public interface FragmentDisplay {
    void showNavigationBottom(boolean show);
    void setDisplayHomeAsUpEnabled(boolean show);
    void updateTitle(String title);
    void showToolbar(boolean show);
    void showFragment(int tag, Fragment parent, Bundle bundle);
    void showLoadingDialog(boolean show);
    void onBackPressed();
    void restartApp();
    void showToast(String content, int time);

}
