package asia.bluebelt.mvp.base;

/**
 * Created by Bluebee on 3/9/2018.
 */

public interface BaseContract {
    // handle user's action.
    interface Presenter {
    }

    // show data to UI
    interface View {

    }
}
