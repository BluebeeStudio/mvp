package asia.bluebelt.mvp.base;

/**
 * Created by Bluebee on 3/8/2018.
 */

public abstract class BasePresenter<U extends BaseContract.View> {
    private U mUi;

    public U getUi() {
        return mUi;
    }

    public void setUi(U mUi) {
        this.mUi = mUi;
    }

    public abstract void onStart();
    public abstract void onStop();
}
