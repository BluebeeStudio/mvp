package asia.bluebelt.mvp.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import asia.bluebelt.mvp.FragmentDisplay;


/**
 * Created by Bluebee on 3/8/2018.
 */

public abstract class BaseFragment<T extends BasePresenter,U extends BaseContract.View> extends Fragment  {
    private T mPresenter;
    public abstract T createPresenter();
    public abstract U getUi();

    public T getPresenter() {
        return mPresenter;
    }

    public FragmentDisplay mFragmentDisplay;

    private void intFragment(){
        mFragmentDisplay = (FragmentDisplay)getActivity();
        mPresenter = createPresenter();
        mPresenter.setUi(getUi());
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intFragment();
    }

    public Fragment getParent(){
        Fragment fragment = getParentFragment();
        while(fragment != null){
            if(fragment.getParentFragment() != null){
                fragment = fragment.getParentFragment();
            }else{
                break;
            }
        }
        return fragment;
    }


    public boolean needUpdate(){
        Fragment fragment = getParent();
        return fragment!=null && fragment.isVisible();
    }
}
