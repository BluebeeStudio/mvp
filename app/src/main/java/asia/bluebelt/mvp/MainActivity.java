package asia.bluebelt.mvp;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements FragmentDisplay{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void showNavigationBottom(boolean show) {

    }

    @Override
    public void setDisplayHomeAsUpEnabled(boolean show) {

    }

    @Override
    public void updateTitle(String title) {

    }

    @Override
    public void showToolbar(boolean show) {

    }

    @Override
    public void showFragment(int tag, Fragment parent, Bundle bundle) {

    }

    @Override
    public void showLoadingDialog(boolean show) {

    }

    @Override
    public void restartApp() {

    }

    @Override
    public void showToast(String content, int time) {

    }
}
